
/**
 * WsConsultaEspacialGDESkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.3  Built on : Aug 10, 2007 (04:45:47 LKT)
 * with extensions for GE Smallworld GeoSpatial Server
 */
    package co.net.une.www.svc;
    import java.util.Map;
    import java.util.HashMap;    
    
    import org.apache.axis2.engine.AxisError;
    
    import com.gesmallworld.gss.lib.exception.GSSException;
    import com.gesmallworld.gss.webservice.WebServiceRequest;
    /**
     *  WsConsultaEspacialGDESkeleton java skeleton for the axisService
     */
    public class WsConsultaEspacialGDESkeleton extends WebServiceRequest
        {
        
	
	private static final String serviceName = "ejb/WsConsultaEspacialGDELocal";
	
     
         
        /**
         * Auto generated method signature
         
         
                                     * @param latitud
                                     * @param longitud
         */
        

                 public co.net.une.www.gis.WsConsultaEspacialGDERSType consultaEspacial
                  (
                  java.lang.String latitud,java.lang.String longitud
                  )
            {
                //GSS generated code
		Map<String,Object> params = new HashMap<String,Object>();
                  params.put("latitud",latitud);params.put("longitud",longitud);
		try{
		
			return (co.net.une.www.gis.WsConsultaEspacialGDERSType)
			this.makeStructuredRequest(serviceName, "consultaEspacial", params);
		}catch(GSSException e){
                    // Modify if specific faults are required
                    throw new AxisError(e.getLocalizedMessage()+": "+e.getRootThrowable().getLocalizedMessage(), e.getRootThrowable());
                }
        }
     
    }
    